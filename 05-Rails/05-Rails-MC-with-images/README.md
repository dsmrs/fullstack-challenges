## Background & Objectives

Today, we'll take our Mister Cocktail app and enhance it with some images.

### Specs

The goal is to add a picture to the cocktail. The user should be able to
upload an image. The image would then be displayed on the `index` view
of `Cocktail`, as thumbnails. On the `show` view of `Cocktail`, the same
image should be displayed, but bigger!

Even with such a simple app, try to make something beatiful using Bootstrap,
the right fonts and an ounce of creativity :)

### Deployment

Make sure uploading works both in the `development` environment (on your laptop)
and on the `production` environment (Heroku)