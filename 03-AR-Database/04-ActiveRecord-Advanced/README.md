### Roadmap of the day

#### Set-up

Before starting the challenges, ensure you have a clean git status and that you have pulled upstream. Otherwise you may work on old versions of the challenges.

```
$ cd ~/code/${GITHUB_USERNAME}/fullstack-challenges/
$ git status # everything should be ok!
$ git pull --no-edit upstream master
```

Ensure you're connected on Slack.

#### `01-Associations`

This challenge will make you create a new migration and its related model. You'll learn how to connect two models together.

#### `02-Validations`

The goal of this exercise is to add validations on attributes of the models. You'll learn how to prevent insertions/updates in database if your models aren't valid.

#### `03-Sinatra`

Let's replace our terminal output by a website! In this exercise, you'll discover how to create your first dynamic website page. You'll have to retrieve all the posts from the database and display them on your homepage.
