### Reboot or not reboot?
Today's challenges are rather difficult javascript exercises. If you don't feel comfortable in jQuery and AJAX, you'd better spend some time re-doing previous challenges of the week.

#### `01-form-validation`
When people are filling your form, it's best to give them the feedback as quick as possible if some field is badly filled. That's called client-side validation and it's a classical use of javascript.

#### `02-Minesweeper`
A less classical use of JS: build a minesweeper game!
We're back in good old day, yo!