#### `01-Profile-content`
A simple challenge to manipulate basic HTML tags and create a profile page with headers/texts/lists/images/tables.

#### `02-Fonts-and-colors`
Add simple CSS rules to design fonts and colors of your profile page.

#### `03-Box-model`
Play with the box model (`margin/border/padding/width/height`) by splitting your profile informations into different `<div>`.

#### `04-Advanced-selectors`
Use advanced selectors (id, class, grouping, descendant selectors) to fine-tune your page with a more subtle design.

#### `05-Fixed-sidebar`
Build a nice layout with a fixed sidebar and a scrollable page content.

#### `06-Profile-dashboard`
Create a cool profile dashboard by inserting your infos into a nice 2D layout. You'll
have to use a lot of CSS positionning techniques.

#### `06-Event-form`
A little challenge to manipulate HTML `<form>` and most standard `<input>`.