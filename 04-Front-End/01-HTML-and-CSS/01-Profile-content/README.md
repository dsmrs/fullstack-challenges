## Background & Objectives

A simple challenge to manipulate basic HTML tags and create a profile page with headers/texts/lists/images/tables.

Note: You can start a local webserver by typing the following command in your terminal (just make sure you are in the exercises folder):

```bash
$ serve
```

(it has been defined in an [alias](https://github.com/lewagon/dotfiles/blob/f894306fd81502f1fe513dd253e3129f4b56874d/aliases#L7))

You can now visit your files on [http://localhost:8000](http://localhost:8000)

## Specs

Build a simple HTML page with the following elements (use the right HTML tags):

- an image of yourself
- a header and sub-header with your name and position
- a description of yourself
- a list of your social links
- a table of your favorite movies, sport teams, recipes, etc... depending on your hobbies.

A picture is worth a thousand words, [here is what you should build in this challenge](http://lewagon.github.io/html-css-challenges/01-profile-content/)

## Tips & Resources

- Prepare your image files (1 profile picture + 3 movie pictures) before starting to code. Ensure your profile image is square (crop it if it's not the case) and ensure your movies pictures are all the same width in pixels.
- Don't forget the basic skeleton tags `<html>`, `<body>`, `<head>`.
- Don't forget the page `<title>` in the `<head>` section, and other important metatags like `<meta charset="utf-8">`.
- You can use [Font Awesome](http://fortawesome.github.io/Font-Awesome/) to find nice icons (e.g. for social networks). It is a very convenient library since it is a **font** of icons (hence it will be very easy to resize these icons, change their color, and add animations on them!)
- You can use the `target="_blank"` attribute on your links to open them on new tabs once clicked.
