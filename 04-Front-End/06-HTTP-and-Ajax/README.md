#### `01-promo-chat`
Implement a dynamic chat-room for your promotion using AJAX requests.

#### `02-Facebook-graph-API`
Use the Facebook Graph API to build a dynamic webpage showing all your facebook infos (firstname, lastname, picture, friends..)

#### `Optional-01-AJAX-wagon-race`
Let's plug our wagon race game to an API to record our game sessions and scores.